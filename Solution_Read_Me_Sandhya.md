Read Me for solution:

### List of python files:
1. "home.py" for creating the database and defining the schema which are a part of Task 1
2. "formatting.py" for formatting the data and populating it into the database from entries.json which are a part of Task 1
3. "actions.py" has endpoints for all other tasks 

### Endpoint Forms
1. view.html - to add a new book onto database - If insertion is successful, an output message "New record inserted" is displayed
2. viewAllIds.html - to view all the available IDs
3. viewbyidform.html - to enter the ID of the book you wish to view
4. viewbyauthor.html - to enter the name of the author and view his books

###  Templates
All the HTML templates which display output are stored in 'templates' folder

1. table.html - shows all the available IDS
2. tablebyid.html - shows items specific to the entered ID
3. tablebyauthor.html - shows the lists of books specific to the entered Author
4. tabletotal.html - shows the total price of the books listed in tablebyauthor.html

### Schema:
Schema is stored as JSON file name databaseSchema.json

### Steps to run the server

1. Open Command PRompt and run the below command:

python actions.py

then our flask app starts running, now open the below HTML pages:

1. view.html - to add a new book onto database - If insertion is successful, an output message "New record inserted" is displayed
2. viewAllIds.html - to view all the available IDs
3. viewbyidform.html - to enter the ID of the book you wish to view, you'll be redirected to the result page
4. viewbyauthor.html - to enter the name of the author and view his/her books, you'll be redirected to the result page


### Task 4:

If we want to distinguish books by different authors that share the same name, we can cite the publish date so it serves as a differentiating factor, i.te., we can cite the ir work in standard author-date format.