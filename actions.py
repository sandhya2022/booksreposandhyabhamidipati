# list of all imports
from flask import Flask, render_template, request
import json
import boto3
import os
import uuid
import time
from boto3.dynamodb.conditions import Key, Attr
total = 0

# create a session from credentials in the document provided namely settings.py
app = Flask(__name__)
session = boto3.Session(
    aws_access_key_id='AKIAX5EQC3DNZ52ZELAN',
    aws_secret_access_key='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',
    region_name = 'eu-central-1'
)

#using sesion to get DynamoDB session
dynamodb = session.resource('dynamodb')

#Task - 1: create a DynamoDB Table
# Please refer to home.py for creation of DB along with its schema
# Please refer to formatting.py for populating database from entries.json
table = dynamodb.Table('coding_challenge_sandhya_sirisha')

# Task-2 part 1: Endpoint to add an entry to the database
@app.route('/addItem',methods = ['POST', 'GET'])
def result():
    if request.method == 'POST':
        author = request.form.get("author")
        name = request.form.get("name")
        publish_date = request.form.get("publish date")
        price = request.form.get("price")
        id = str(uuid.uuid4())
        timestamp = int(time.time())

        
        response = table.put_item(
           Item={
               'id': id,
               'timestamp' : timestamp,
                'name': name,
                'author': author,
                'publish date': publish_date,
                'price': price,
               }
        )

    print(response)
    return "New record inserted"

# Task 2 part 2- Endpoint to view all available IDs
@app.route('/viewAllIds',methods = ['GET'])
def result1():
    if request.method == 'GET':
       response = table.scan()
       allIds = response["Items"]
       available_IDs = {}
       tup = (item.get('id') for item in allIds)
       available_IDs["available_IDs"] = tup
       headings = "available_IDs"
       data = tup
       print(allIds)
    return render_template("table.html", headings=headings, data= data)


# Task 2 part 3 - Endpoint to get a specific item by ID
@app.route('/viewById',methods = ['POST'])
def result2():
    if request.method == 'POST':
       id_ = request.form.get("id")
       print('type',type(id_))
       response = table.query(
           KeyConditionExpression=Key('id').eq(id_)
        )
       print(response)
       headings = ('timestamp', 'id','price', 'name', 'publish date', 'author' )
       item = response["Items"][0]
       tup = (str(item[key]) for key in headings)
       data = []
       data.append(tup)
       
       print(data[0]) 
    return render_template("tablebyid.html", headings=headings, data= tuple(data))

# Task 3 part 1- Endpoint to get a filtered list of entries by author
'''
@app.route('/viewByAuthor',methods = ['POST'])
def result3():
    if request.method == 'POST':
       author = request.form.get("author")
       #print('type',type(id_))
       response = table.scan(
           FilterExpression=Attr('author').eq(author)
        )
       print(response)
       headings = ('timestamp', 'id','price', 'name', 'publish date', 'author' )
       items = response["Items"]
       print(items)
       data = []
       total = 0
       #tup = (str(item[key]) for item in items_ for key in headings)
       for item in items:
          
           tup = (str(item[key]) for key in headings)
           total += float(item['price'])

           data.append(tuple(tup))

    return render_template("tablebyauthor.html", headings=headings, data= tuple(data))
'''


# Task 3 part 2,3 - Modify earlier endpoint to list out only name and price of books
# Along with calculating the total price of the books by author
@app.route('/viewByAuthor',methods = ['POST', 'GET'])
def result3():

    if request.method == 'POST':
       author = request.form.get("author")
       response = table.scan(
           FilterExpression=Attr('author').eq(author)
        )
       print(response)
       headings = ('price', 'name' )
       items = response["Items"]
       print(items)
       data = []
       for item in items:
           global total
           tup = (str(item[key]) for key in headings)
           total += float(item['price'])

           data.append(tuple(tup))
       print(total)

       return render_template("tablebyauthor.html", heading=headings, data= tuple(data), total = total)

# Endpoint for displaying the price of books in above task
@app.route('/getTotalPrice',methods = ['GET'])
def result4():
    return render_template("tabletotal.html", query = total)

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=1050,debug = True)