# Introduction:
This coding challenge is centered around building an rest API to manage entries in a book database. 
You will use the Python Flask web framework to build an API around an AWS Dynamo Database.

## Tools needed
* Git
* Python3 + pip (or any other package manager)
* An IDE of your choice (we use vs code @ agrilution)

## Setup
* Clone repository
* Usually you want to create a virtual environment (you don't have to). If need be:
* Create virtual environment e.g.: `python -m venv .env`
* Activate virtual environment in console e.g.:
    * `.env/Scripts/activate.ps1` // Windows
    * `source .env/bin/activate` // Linux/Mac
* `pip install -r requirements.txt`

If you use any packages not specified in requirements.txt please add them to the file and commit it with your code.


## Coding Challenge
The coding challenge is divided in multiple subtasks some of which are dependent on each other.
Task 1-3 are mandatory. Task 4-7 are optional.  
Work your way down from the top for the first 3 tasks. Afterwards, complete them in any order.
Continue with the ones you feel most conformable with. Remember to git commit after each task.

Remember that the optional tasks are really optional, it is not a necessity to commit all of those, rather focus on having those tasks that you do done nicely.

Evaluation criteria:

* Sticking to best practices of python and the used libraries  
* Clean comments (e.g. Docstrings)  
* Clean/readable code  
* Commiting after each task  
* Usability (e.g. adapt the Readme and/or requirements.txt if you change anything in the setup)


## Task 1: Create Table
In the first task you will create a DynamoDB table and populate the first entries.

### Find the boto3 documentation here  
* https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html

### Model

| Field        | Type         | Additional information      |
|--------------|--------------|-----------------------------|
| id           | CharField    | Random UUID-4, Primary key  |
| timestamp    | IntegerField | Unix Timestamp when entry was added to database. Sort Key |
| name         | CharField    | Name of Book.               |
| author       | CharField    | Name of Author              |
| publish date | Date         | The year the book was published |
| price        | DecimalField | Price of the book in Euro   |

(as well as any additional fields you might find feasible to add during this task)

### Deliverable
Connect to an AWS instance using the credentials provided in settings.py.  
The api key gives you access to create/modify/delete any database that starts with the name "coding_challenge_".

Your task is to create the table as described above (you only need to specify the primary and sort key) and populate the created table with the entries delivered within entries.json.  
Save the database schema as yml or json into the repository and commit it together with the code you used to create and populate the database from entries.json to bitbucket.


## Task 2: Create API endpoints & frontend
Create a simple web server and hook up endpoints for:

* Creating a new item
* Getting a list of all available ids
* Get a specific item by id

Then create a frontend for the results and input of these endpoints. 

You can use Flask for the web server and HTML templates for the frontend. Flask configures the Jinja2 template engine for you automatically. However, if you rather build your web server and frontend with another library feel free to do so. 

### Deliverable
Provide all neccessary steps to start the web sever in the readme under the point Task 2. Design your web page (frontend) in a simple but self explaining way. Commit your code for the web server, the frontend, and the readme to bitbucket.

## Task 3: Add filter  
* Create another endpoint to get a filtered list of entries by author  
* Create another endpoint to get the price sum of the filtered list of the previous subtask  
* Modify the first endpoint of Task 3 to specify the fields which are returned in the list (e.g. return only name and price of the books of author x but no other fields like timestamp, publish date, etc.)

### Deliverable
Commit your code for the additional endpoints to bitbucket.

## Task 4: What about the author
We want to distinguish books by different authors that share the same name. How can we do this? To answer this write a proposal within this readme under the point Task 4. No need to implement.

### Deliverable
Commit the readme file with your answer to bitbucket.

## Task 5: Unit Test :)
Write unit tests for your endpoints. You can use Moto or also python's unittest mock for mocking and unit testing AWS resources. A unit test skeleton is already provided for you in the "test" folder.  
To execute all unittests run `python -m unittest discover` from your repository directory.  
To execute a specific file use `python -m unittest test.test_main` and for a specific test `python -m unittest test.test_main.BasicTests.test_main_page`

### Deliverable
Commit your code for the unit tests to bitbucket.

## Task 6: Getting fuzzy
* Modify the endpoint for the filtered list of Task 3 to get the filtered list even if we only know part of the authors name (e.g. searching for "carl" should retrieve all authors that have "carl" somewhere in their name)  
* Bonus: find similar sounding or looking names. E.g. still find an author if you have a typo in the search  

### Deliverable
Commit your code to bitbucket.

##  Task 7: Architectural dive
Get familiar with AWS S3. 
Think of an architecture how we can extend the "create item" request of Task 2 with the possibility to upload one or more images belonging to the book. The images can have different affiliations to the book (e.g one image is the cover image, and three others are the book preview of the first 3 pages). Figure out a way to store the information about which book the image belongs to, as well as what affiliation the image has to the book, efficiently and easily.  
We want to use a two-step process: The "create item" request shall return information which then can be used to push images to S3. Storing information about the images can be done in different ways in S3, but also in DynamoDB. Choose a way that seems most suitable in you opinion.

### Deliverable
The deliverable of this task is an architectural concept, either as diagram, text or a mixture of both. No need for implementation here.  
Commit your answer as PDF or within this readme to bitbucket.

---
Python Backend Coding Challenge: Book Database API
(c) 2021 Agrilution, written by Jakob Oelkers and Daniela Hauber