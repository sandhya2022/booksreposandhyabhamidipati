import json
import uuid
import time

formatted={}
formatted["coding_challenge_sandhya_sirisha"]=[]
with open('entries.json', 'r') as myfile:
    data=myfile.read()

# parse file
books = json.loads(data)
for book in books["books"]:
    id_ ={}
    id_["S"] = str(uuid.uuid4())
    timestamp = {}
    timestamp["N"] = int(time.time())
    name = {}
    name["S"] = book["name"] 
    author={}
    author["S"] = book["author"]
    published_date = {}
    published_date["N"] = book["publish date"]
    price = {}
    price["N"] = book["price"]


    entry={}
    entry["PutRequest"]={}
    entry["PutRequest"]["Item"]={}
    entry["PutRequest"]["Item"]["name"] = name
    entry["PutRequest"]["Item"]["author"] = author
    entry["PutRequest"]["Item"]["publish date"] = published_date
    entry["PutRequest"]["Item"]["price"] = price


    formatted["coding_challenge_sandhya_sirisha"].append(entry)


print(formatted,type(formatted))
json_object = json.dumps(formatted, indent = 4)
  
# Writing to sample.json
with open("coding_challenge_sandhya_sirisha.json", "w") as outfile:
    outfile.write(json_object)
