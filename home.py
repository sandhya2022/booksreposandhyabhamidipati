import boto3
import json
import requests
import time

# Task-1 - create table with given partition and sort keys
client = boto3.client(
    'dynamodb',
    aws_access_key_id='AKIAX5EQC3DNZ52ZELAN',
    aws_secret_access_key='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',
    region_name='eu-central-1'
    )
dynamodb = boto3.resource(
    'dynamodb',
    aws_access_key_id='AKIAX5EQC3DNZ52ZELAN',
    aws_secret_access_key='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',
    region_name='eu-central-1'
    )
ddb_exceptions = client.exceptions

try:
    table = client.create_table(
        TableName='coding_challenge_sandhya_sirisha',
        KeySchema=[
            {
                'AttributeName': 'id',
                'KeyType': 'S' #Partition key
            },
            {
                'AttributeName': 'timestamp',
                'KeyType': 'S'  #sort key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'id',
                'AttributeType': 'HASH' #Partition key
            },
            {
                'AttributeName': 'timestamp',
                'AttributeType': 'RANGE'  #sort key
            }
    
        ]
    )
    print("Creating table")
    waiter = client.get_waiter('table_exists')
    waiter.wait(TableName='coding_challenge_sandhya_sirisha')
    print("Table created")
    
except ddb_exceptions.ResourceInUseException:
    print("Table exists")


Table_data = {}

table = dynamodb.Table('coding_challenge_sandhya_sirisha')

response = table.scan()
data = response['Items']

while 'LastEvaluatedKey' in response:
    response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
    data.extend(response['Items'])