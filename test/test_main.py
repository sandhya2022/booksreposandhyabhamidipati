import os
import unittest
from moto import mock_dynamodb2
from unittest import mock
import main

class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################
    
    app = None

    def setUp(self):
        """executed prior to each test
        """
        self.app = main.app.test_client()
        self.app.testing = True
        # TODO setup any mocks needed

    def tearDown(self):
        """executed after each test
        """
        # TODO delete any used mocks
        pass
    
    ###############
    #### tests ####
    ###############
    
    def test_main_page(self):
        """test if main page is running
        """
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    # TODO: add your tests here
    

if __name__ == "__main__":
    unittest.main()